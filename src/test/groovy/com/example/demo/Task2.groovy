package com.example.demo

import com.example.demo.catalog.domain.items.entity.ItemEntitySpec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task2 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ItemEntitySpec.class)
        List testNames = [
                "Item Entity all parameters constructor test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)",
                "Item Entity no parameters constructor test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)",
                "GetId test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)",
                "SetId test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)",
                "GetName test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)",
                "SetName test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)",
                "GetPrice test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)",
                "SetPrice test(com.example.demo.catalog.domain.items.entity.ItemEntitySpec)"
        ]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
