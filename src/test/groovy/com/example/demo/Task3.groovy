package com.example.demo

import com.example.demo.catalog.domain.ItemRepositorySpec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task3 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ItemRepositorySpec.class)
        List testNames = ["save Item Entity(com.example.demo.catalog.domain.ItemRepositorySpec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
