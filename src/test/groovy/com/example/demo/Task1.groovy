package com.example.demo


import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task1 extends Specification{

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ProjectSetupSpec.class)
        List testNames = [
                "should return true(com.example.demo.ProjectSetupSpec)"
        ]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
