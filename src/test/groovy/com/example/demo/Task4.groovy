package com.example.demo

import com.example.demo.catalog.domain.ItemService1Spec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task4 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ItemService1Spec.class)
        List testNames = ["is item Entity saved(com.example.demo.catalog.domain.ItemService1Spec)",
                          "is item Entity retrieved(com.example.demo.catalog.domain.ItemService1Spec)",
                          "should return null in there is no item entity with requested id(com.example.demo.catalog.domain.ItemService1Spec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
