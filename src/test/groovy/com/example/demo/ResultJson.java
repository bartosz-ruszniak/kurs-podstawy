package com.example.demo;

import java.util.List;

class ResultJson {
    private double StandardMark;
    private int TotalTestsNumber;
    private int ExecutedTestsNumber;
    private int PassedTestsNumber;
    private int FailedTestsNumber;
    private List<PassedTestResultJson> PassedTests;
    private List<FailedTestResultJson> FailedTests;

    public double getStandardMark() {
        return StandardMark;
    }

    public void setStandardMark(double standardMark) {
        StandardMark = standardMark;
    }

    public int getTotalTestsNumber() {
        return TotalTestsNumber;
    }

    public void setTotalTestsNumber(int totalTestsNumber) {
        TotalTestsNumber = totalTestsNumber;
    }

    public int getExecutedTestsNumber() {
        return ExecutedTestsNumber;
    }

    public void setExecutedTestsNumber(int executedTestsNumber) {
        ExecutedTestsNumber = executedTestsNumber;
    }

    public int getPassedTestsNumber() {
        return PassedTestsNumber;
    }

    public void setPassedTestsNumber(int passedTestsNumber) {
        PassedTestsNumber = passedTestsNumber;
    }

    public int getFailedTestsNumber() {
        return FailedTestsNumber;
    }

    public void setFailedTestsNumber(int failedTestsNumber) {
        FailedTestsNumber = failedTestsNumber;
    }

    public List<PassedTestResultJson> getPassedTests() {
        return PassedTests;
    }

    public void setPassedTests(List<PassedTestResultJson> passedTests) {
        PassedTests = passedTests;
    }

    public List<FailedTestResultJson> getFailedTests() {
        return FailedTests;
    }

    public void setFailedTests(List<FailedTestResultJson> failedTests) {
        FailedTests = failedTests;
    }
}
