package com.example.demo

import com.example.demo.ProjectSetup
import spock.lang.Specification

class ProjectSetupSpec extends Specification {

    ProjectSetup projectSetup = new ProjectSetup()

    def "should return true"() {
        expect:
        projectSetup.returnTrue()
    }
}
