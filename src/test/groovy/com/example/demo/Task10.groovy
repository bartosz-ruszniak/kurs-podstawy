package com.example.demo


import com.example.demo.catalog.web.rest.ItemController3Spec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task10 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ItemController3Spec.class)
        List testNames = ["should get ApiError with code OBJECT_NOT_FOUND and status 404(com.example.demo.catalog.web.rest.ItemController3Spec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
