package com.example.demo;

class FailedTestResultJson {
    private String testName;
    private String errorMessage;
    private String errorStackTrace;

    public FailedTestResultJson(String testName, String errorMessage, String errorStackTrace) {
        this.testName = testName;
        this.errorMessage = errorMessage;
        this.errorStackTrace = errorStackTrace;
    }
    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorStackTrace() {
        return errorStackTrace;
    }

    public void setErrorStackTrace(String errorStackTrace) {
        this.errorStackTrace = errorStackTrace;
    }
}
