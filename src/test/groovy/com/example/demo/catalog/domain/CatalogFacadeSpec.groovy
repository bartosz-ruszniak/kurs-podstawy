package com.example.demo.catalog.domain


import com.example.demo.catalog.domain.items.entity.ItemEntity
import spock.lang.Specification

class CatalogFacadeSpec extends Specification {

    private ItemRepository itemRepository = Stub(ItemRepository)
    private CatalogFacade catalogFacade = new CatalogFacade(new ItemService(itemRepository))

    def "createItem test"() {
        given:
        def itemEntity = new ItemEntity("a", BigDecimal.TEN)
        itemRepository.save(itemEntity) >> itemEntity

        when:
        ItemEntity item = catalogFacade.createItem(itemEntity)

        then:
        item.name == "a"
        item.price == BigDecimal.TEN
    }

    def "getItem test"() {
        given:
        def itemEntity = new ItemEntity(23,"a", BigDecimal.TEN)
        itemRepository.findById(itemEntity.getId()) >> Optional.of(itemEntity)

        when:
        ItemEntity item = catalogFacade.getItem(itemEntity.getId())

        then:
        item.id == 23
        item.name == "a"
        item.price == BigDecimal.TEN
    }
}
