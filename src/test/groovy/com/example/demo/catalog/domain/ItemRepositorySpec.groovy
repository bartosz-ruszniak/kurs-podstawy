package com.example.demo.catalog.domain

import com.example.demo.catalog.domain.items.entity.ItemEntity
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import spock.lang.Specification

@DataJpaTest
class ItemRepositorySpec extends Specification {

    @Autowired
    ItemRepository itemRepository

    def "save Item Entity"() {
        when:
        def save = itemRepository.save(new ItemEntity(1, "mock", BigDecimal.TEN))
        then:
        save != null
    }

}
