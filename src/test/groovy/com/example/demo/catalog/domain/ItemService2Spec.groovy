package com.example.demo.catalog.domain

import com.example.demo.catalog.domain.items.entity.ItemEntity
import com.example.demo.core.exception.ObjectNotFoundException
import com.example.demo.core.exception.ValidationException
import spock.lang.Specification

class ItemService2Spec extends Specification {

    ItemRepository itemRepository = Stub(ItemRepository)
    ItemService itemService = new ItemService(itemRepository)

    def "is item Entity saved"() {
        given:
        def itemEntity = new ItemEntity("a", BigDecimal.TEN)
        itemRepository.save(itemEntity) >> itemEntity

        when:
        ItemEntity item = itemService.createItem(itemEntity)

        then:
        item.name == "a"
        item.price == BigDecimal.TEN
    }

    def "should throw ValidationException when entity has id is not null"() {
        given:
        def itemEntity = new ItemEntity(24,"a", BigDecimal.TEN)
        itemRepository.save(itemEntity) >> itemEntity

        when:
        itemService.createItem(itemEntity)

        then:
        thrown ValidationException
    }

    def "is item Entity retrieved"() {
        given:
        def itemEntity = new ItemEntity(23,"a", BigDecimal.TEN)
        itemRepository.findById(itemEntity.getId()) >> Optional.of(itemEntity)

        when:
        ItemEntity item = itemService.getItem(itemEntity.getId())

        then:
        item.id == 23
        item.name == "a"
        item.price == BigDecimal.TEN
    }

    def "should throw ObjectNotFoundException when there is no item entity with requested id"() {
        given:
        def itemEntity = new ItemEntity(23,"a", BigDecimal.TEN)
        itemRepository.findById(1) >> Optional.of(itemEntity)

        when:
        ItemEntity item = itemService.getItem(itemEntity.getId())

        then:
        thrown ObjectNotFoundException
    }
}
