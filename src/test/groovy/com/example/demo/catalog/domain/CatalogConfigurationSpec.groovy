package com.example.demo.catalog.domain


import com.example.demo.catalog.domain.items.entity.ItemEntity
import spock.lang.Specification

class CatalogConfigurationSpec extends Specification {

    private ItemRepository itemRepository = Stub(ItemRepository)

    def "configuration setup"() {
        given:
        def itemEntity = new ItemEntity("a", BigDecimal.TEN)
        itemRepository.save(itemEntity) >> itemEntity
        CatalogConfiguration configuration = new CatalogConfiguration()

        when:
        CatalogFacade facade = configuration.getCatalogFacade(itemRepository)
        def item = facade.createItem(itemEntity)

        then:
        item.name == "a"
        item.price == BigDecimal.TEN
    }
}
