package com.example.demo.catalog.domain.items.entity

import spock.lang.Specification

class ItemEntitySpec extends Specification {

    def "Item Entity all parameters constructor test"() {
        when:
        def entity = new ItemEntity(1, "test", BigDecimal.TEN)
        then:
        entity.id == 1
        entity.name == "test"
        entity.price == BigDecimal.TEN
    }

    def "Item Entity no parameters constructor test"() {
        when:
        def entity = new ItemEntity()
        then:
        1 == 1
    }

    def "GetId test"() {
        when:
        def entity = new ItemEntity(1, "test", BigDecimal.TEN)
        then:
        entity.getId() == 1
    }

    def "SetId test"() {
        given:
        def entity = new ItemEntity(1, "test", BigDecimal.TEN)
        when:
        entity.setId(2)
        then:
        entity.id == 2
    }

    def "GetName test"() {
        when:
        def entity = new ItemEntity(1, "test", BigDecimal.TEN)
        then:
        entity.getName() == "test"
    }

    def "SetName test"() {
        given:
        def entity = new ItemEntity(1, "test", BigDecimal.TEN)
        when:
        entity.setName("test 2")
        then:
        entity.name == "test 2"
    }

    def "GetPrice test"() {
        when:
        def entity = new ItemEntity(1, "test", BigDecimal.TEN)
        then:
        entity.getPrice() == BigDecimal.TEN
    }

    def "SetPrice test"() {
        given:
        def entity = new ItemEntity(1, "test", BigDecimal.TEN)
        when:
        entity.setPrice( BigDecimal.TEN)
        then:
        entity.price == BigDecimal.TEN
    }
}
