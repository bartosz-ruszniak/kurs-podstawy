package com.example.demo.catalog.web.rest


import com.example.demo.catalog.domain.CatalogFacade
import com.example.demo.catalog.domain.items.entity.ItemEntity
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class ItemController1Spec extends Specification {

    private MockMvc mvc
    private CatalogFacade catalogFacade = Mock(CatalogFacade)
    private ObjectMapper objectMapper = new ObjectMapper()

    void setup() {
        mvc = MockMvcBuilders.standaloneSetup(new ItemController(catalogFacade)).build()
    }

    def 'should create item'() {
        def requestUnderTest = '''{
            "id": "22",
            "name": "item",
            "price": "2500"
            }'''

        1 * catalogFacade.createItem { it ->
            it.id == 22 && it.name == "item" && it.price == BigDecimal.valueOf(2500)
        } >> new ItemEntity(1, "item", BigDecimal.valueOf(2500))

        when:
        def result = mvc
                .perform(post('/items')
                        .contentType(APPLICATION_JSON)
                        .content(requestUnderTest))
                .andExpect(status().isOk())
                .andReturn()
                .response

        then:
        def response = objectMapper.readValue(result.contentAsString, ItemEntity)
        response.id == 1
        response.name == "item"
        response.price == 2500
    }

    def 'should get item'() {
        given:
        1 * catalogFacade.getItem(22) >>
                new ItemEntity(22, "item", BigDecimal.valueOf(2500))

        when:
        def result = mvc
                .perform(get('/items/22')
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn()
                .response

        then:
        def response = objectMapper.readValue(result.contentAsString, ItemEntity)
        response.id == 22
        response.name == "item"
    }
}
