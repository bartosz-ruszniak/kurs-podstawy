package com.example.demo.catalog.web.rest

import com.example.demo.DemoApplication
import com.example.demo.core.exception.ApiError
import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.web.servlet.MockMvc
import spock.lang.Specification

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.MOCK
import static org.springframework.http.MediaType.APPLICATION_JSON
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = MOCK)
@ContextConfiguration(classes = [DemoApplication])
class ItemController3Spec extends Specification {

    @Autowired
    private MockMvc mvc

    @Autowired
    private ObjectMapper objectMapper

    def 'should get ApiError with code OBJECT_NOT_FOUND and status 404'() {
        when:
        def result = mvc
                .perform(get('/items/22')
                        .contentType(APPLICATION_JSON))
                .andExpect(status().isNotFound())
                .andReturn()
                .response

        then:
        def response = objectMapper.readValue(result.contentAsString, ApiError)
        response.errorCode.toString() == "OBJECT_NOT_FOUND"
    }
}
