package com.example.demo

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.runner.Result
import org.springframework.util.StringUtils

import java.util.stream.Collectors

class Utils {

    static void saveToJson(Result result, List<String> testNames) {
        BufferedWriter writer = null
        try {
            writer = new BufferedWriter(new FileWriter("results.json"))
            ObjectMapper objectMapper = new ObjectMapper()
            testNames.removeAll(result.getFailures()
                    .stream()
                    .map({ it -> it.getTestHeader() })
                    .collect(Collectors.toList()))

            def successes = testNames
                    .stream()
                    .map({ it -> new PassedTestResultJson(it) })
                    .collect(Collectors.toList())

            def failures = result.getFailures()
                    .stream()
                    .map({ it ->
                        new FailedTestResultJson(
                                it.getTestHeader(),
                                StringUtils.delete(it.getMessage(), "\n"),
                                StringUtils.delete(it.getTrace(), "\n"))
                    }).collect(Collectors.toList())

            ResultJson json = new ResultJson()
            json.setStandardMark((result.getRunCount() - result.getFailureCount()) / result.getRunCount())
            json.setTotalTestsNumber(result.getRunCount())
            json.setExecutedTestsNumber(result.getRunCount())
            json.setPassedTestsNumber(result.getRunCount() - result.getFailureCount())
            json.setFailedTestsNumber(result.getFailureCount())
            json.setPassedTests(successes)
            json.setFailedTests(failures)
            String results = objectMapper.writeValueAsString(json)
            writer.write(results)
        } catch (IOException e) {
            throw new RuntimeException("Could not create file")
        } finally {
            if (writer != null) {
                writer.close()
            }
        }
    }
}
