package com.example.demo

import com.example.demo.catalog.web.rest.ItemController2Spec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task8 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ItemController2Spec.class)
        List testNames = ["should create item(com.example.demo.catalog.web.rest.ItemController2Spec)",
                          "should get item(com.example.demo.catalog.web.rest.ItemController2Spec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
