package com.example.demo

import com.example.demo.catalog.domain.ItemService2Spec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task9 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ItemService2Spec.class)
        List testNames = ["is item Entity saved(com.example.demo.catalog.domain.ItemService2Spec)",
                          "should throw ValidationException when entity has id is not null(com.example.demo.catalog.domain.ItemService2Spec)",
                          "is item Entity retrieved(com.example.demo.catalog.domain.ItemService2Spec)",
                          "should throw ObjectNotFoundException when there is no item entity with requested id(com.example.demo.catalog.domain.ItemService2Spec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
