package com.example.demo

import com.example.demo.catalog.domain.CatalogFacadeSpec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task5 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(CatalogFacadeSpec.class)
        List testNames = ["createItem test(com.example.demo.catalog.domain.CatalogFacadeSpec)",
                          "getItem test(com.example.demo.catalog.domain.CatalogFacadeSpec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
