package com.example.demo

import com.example.demo.catalog.domain.CatalogConfigurationSpec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task6 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(CatalogConfigurationSpec.class)
        List testNames = ["configuration setup(com.example.demo.catalog.domain.CatalogConfigurationSpec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
