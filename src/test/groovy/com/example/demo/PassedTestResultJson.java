package com.example.demo;

public class PassedTestResultJson {
    private String testName;

    public PassedTestResultJson(String testName) {
        this.testName = testName;
    }

    public String getTestName() {
        return testName;
    }

    public void setTestName(String testName) {
        this.testName = testName;
    }
}
