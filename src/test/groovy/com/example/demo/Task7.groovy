package com.example.demo

import com.example.demo.catalog.web.rest.ItemController1Spec
import org.junit.runner.Result
import spock.lang.Specification
import spock.util.EmbeddedSpecRunner

class Task7 extends Specification {

    def "task"() {
        expect:
        EmbeddedSpecRunner runner = new EmbeddedSpecRunner()
        runner.setThrowFailure(false)
        Result result = runner.runClass(ItemController1Spec.class)
        List testNames = ["should create item(com.example.demo.catalog.web.rest.ItemController1Spec)",
                          "should get item(com.example.demo.catalog.web.rest.ItemController1Spec)"]
        Utils.saveToJson(result, testNames)
        1 == 1
    }
}
