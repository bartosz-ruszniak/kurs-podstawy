package com.example.demo.catalog.web.rest;

import com.example.demo.catalog.domain.CatalogFacade;
import com.example.demo.catalog.domain.items.entity.ItemEntity;
import com.example.demo.catalog.web.rest.dto.ItemRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
class ItemController {

    private final CatalogFacade catalogFacade;

    public ItemController(CatalogFacade catalogFacade) {
        this.catalogFacade = catalogFacade;
    }

//    @PostMapping(value = "items")
//    ResponseEntity<ItemEntity> createItem(@RequestBody ItemEntity itemEntity) {
//        return ResponseEntity.ok(catalogFacade.createItem(itemEntity));
//    }
//
//    @GetMapping(value = "items/{id}")
//    ResponseEntity<ItemEntity> getItem(@PathVariable Long id) {
//        return ResponseEntity.ok(catalogFacade.getItem(id));
//    }

    @PostMapping(value = "items")
    ResponseEntity<ItemEntity> createItem(@RequestBody ItemRequest itemRequest) {
        ItemEntity itemEntity = new ItemEntity(
                itemRequest.getName(),
                itemRequest.getPrice());

        return ResponseEntity.ok(catalogFacade.createItem(itemEntity));
    }

    @GetMapping(value = "items/{id}")
    ResponseEntity<ItemEntity> getItem(@PathVariable Long id) {
        return ResponseEntity.ok(catalogFacade.getItem(id));
    }
}
