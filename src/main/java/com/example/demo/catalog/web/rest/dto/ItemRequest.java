package com.example.demo.catalog.web.rest.dto;

import java.math.BigDecimal;

public class ItemRequest {
    private String name;
    private BigDecimal price;

    public ItemRequest() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }
}
