package com.example.demo.catalog.domain;

import com.example.demo.catalog.domain.items.entity.ItemEntity;
import com.example.demo.core.exception.ObjectNotFoundException;
import com.example.demo.core.exception.ValidationException;

class ItemService {

    private final ItemRepository itemRepository;

    ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

//    ItemEntity createItem(ItemEntity item) {
//        return itemRepository.save(item);
//    }
//
//    ItemEntity getItem(long id) {
//        return itemRepository.findById(id)
//                .orElse(null);
//    }

    ItemEntity createItem(ItemEntity item) {
        if(item.getId() != null) {
            throw new ValidationException();
        }

        return itemRepository.save(item);
    }

    ItemEntity getItem(long id) {
        return itemRepository.findById(id)
                .orElseThrow(ObjectNotFoundException::new);
    }
}
