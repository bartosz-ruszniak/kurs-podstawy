package com.example.demo.catalog.domain;

import com.example.demo.catalog.domain.items.entity.ItemEntity;

public class CatalogFacade {

    private final ItemService itemService;

    public CatalogFacade(ItemService itemService) {
        this.itemService = itemService;
    }

    public ItemEntity createItem(ItemEntity item) {
        return itemService.createItem(item);
    }

    public ItemEntity getItem(long id) {
        return itemService.getItem(id);
    }
}
