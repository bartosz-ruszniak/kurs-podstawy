package com.example.demo.catalog.domain;

import com.example.demo.catalog.domain.items.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

interface ItemRepository extends JpaRepository<ItemEntity, Long> {
}
