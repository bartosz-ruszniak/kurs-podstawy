package com.example.demo.catalog.domain;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class CatalogConfiguration {

    @Bean
    CatalogFacade getCatalogFacade(ItemRepository itemRepository) {
        return new CatalogFacade(new ItemService(itemRepository));
    }
}
