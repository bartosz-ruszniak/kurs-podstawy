package com.example.demo.core.exception;

public class ApiError {
    private ErrorCode errorCode;
    private String errorMessage;

    private ApiError(ErrorCode errorCode, String errorMessage) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    static ApiError notFound(String errorMessage){
        return new ApiError(ErrorCode.OBJECT_NOT_FOUND, errorMessage);
    }

    private enum ErrorCode {
        OBJECT_NOT_FOUND
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
